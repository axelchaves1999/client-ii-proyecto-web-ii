import React from 'react';

//icons
import iconDelete from '../../assets/img/icons8_delete_bin_32px.png';
import iconEdit from '../../assets/img/icons8_edit_32px_1.png';
//axios
import axios from 'axios';
//navBar component
import { NavBar } from './index';
//footer component
import { Footer } from './index';
import { apiUrl, queries } from './constans';


export class News extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            url: 'http://localhost:3000/Category',
            categories: [],
            user: '',
            news: [],
            tags: [],
            urlGetNews: 'http://localhost:3000/newslist',
            urlGetNewsCategory: 'http://localhost:3000/newsCategory',
            text: ''

        }
    }

    /*
    Este solicita al servidor las categorias registradas en el servidor
      */
    categoryGet = async () => {
        const config = {
            headers: { Authorization: `Bearer ${this.state.user.user.token}` }
        }
        await axios.get(this.state.url, config).then(response => {
            this.setState({ categories: response.data })

        }).catch(err => {
            console.log(err.message)
        })


    }

    /*
  Este metodo solicita las noticias guradadas en el servidor
  */
    newsGet = async () => {
        const config = {
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${this.state.user.user.token}`
            }
        };

        const body = {
            variables: { newsUserId: this.state.user.user._id },
            query: queries.getNews
        };


        axios.post(apiUrl, body, config)
            .then(res => {
                this.setState({ news: res.data.data.news })

            })
            .catch(err => console.log(err.message));


    }


    /*
      Este metodo solicita las noticias guradadas en el servidor por etiqueta
      */
    newsGetByTag = async (tag) => {
        const config = {
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${this.state.user.user.token}`
            }
        };

        const body = {
            variables: {
                newsTagText: tag,
                newsTagUserId: this.state.user.user._id
            },
            query: queries.getNewsByTag
        };


        axios.post(apiUrl, body, config)
            .then(res => {
                const list = [];
                list.concat(res.data.data.newsTag);
                console.log(list[0])
                this.setState({ news: res.data.data.newsTag });
            })
            .catch(err => console.log(err.message));


    }




    /*
      Este metodo solicita las noticias guradadas en el servidor por etiqueta
      */
    newsGetBysearch = async (text) => {

        if (text.length >= 3) {
            const config = {
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${this.state.user.user.token}`
                }
            };

            const body = {
                variables: {
                    newsSearchText: text,
                    newsSearchUserId: this.state.user.user._id
                },
                query: queries.getNewsBySearch
            };


            axios.post(apiUrl, body, config)
                .then(res => {
                    this.setState({ news: res.data.data.newsSearch });
                })
                .catch(err => console.log(err.message));

        }else{
            this.setState({ news: [] })
        }


    }





    /*
  Este metodo solicita las etiquetas guradadas en el servidor
  */
    tagsGet = async (category) => {
        this.setState({ news: [],
            text: '' })
        const urlTags = 'http://localhost:3000/tags';
        const config = {
            headers: { Authorization: `Bearer ${this.state.user.user.token}` },
            params: { "id": category._id }
        }
        await axios.get(urlTags, config).then(response => {
            this.setState({ tags: response.data })
            console.log(this.state.tags);

        }).catch(err => {
            console.log(err.message)
        })
    }


    /*
  Este metodo da formato a al texto de las noticias del servidor
  */
    trimData = (content) => {
        var respuesta = content;
        var longitud = 200;
        if (content.length > longitud) {
            respuesta = content.substr(0, longitud - 3) + "..."
        }
        return respuesta;
    }

    /*
  Este metodo selecciona las categorias guardadas en el servidor
  */
    selectTag = async (tag) => {
        this.setState({ news: [],
            text: '' })
        if (tag) {
            this.newsGetByTag(tag);

        } else {
            this.setState({ tags: [] })
            this.newsGet();

        }
    }


    /*
      Este metodo utilizado en react nos permite extraer valoes de una campo de texto
      */
    handleChange = async e => {
        e.persist();
        await this.setState({ text: e.target.value });
        this.newsGetBysearch(this.state.text);

    }



    /*
    Este metodo ejecuta las funciones de obtener categoria y obtener noticias
    una vez el componente renderice
    */
    componentDidMount() {
        this.categoryGet();
        this.newsGet();

    }


    /*
  Este metodo recoge el usuario logueado
  */
    getUserLogin() {
        const userLogin = JSON.parse(localStorage.getItem('user'));
        this.state.user = ({ user: userLogin });
    }




    render() {
        const { user } = this.state;
        if (user.length == 0) {
            this.getUserLogin();
        }

        const { text } = this.state;
        return (
            <div className="container">
                <NavBar />
                <div className="containter">
                    <div className="container mt-5 d-flex align-items-center flex-column bg-white text-secondary ">
                        <h4 className="display-6 text-center">Your Unique News Cover</h4>
                        <hr className="my-4 bg-secondary w-25" />
                    </div>

                </div>

                {/* categories */}
                <div className="container  mb-5 pb-5 d-flex flex-column align-items-center w-75">
                    <div className="card-group ">
                        <button type="button" className="btn btn-secondary  m-1" onClick={() => { this.selectTag(null); }}>Portada</button>
                        {this.state.categories.map(category => {
                            return (
                                <button type="button" className="btn btn-secondary  m-1" onClick={() => { this.tagsGet(category); }} >{category.name}</button>
                            );
                        })}
                    </div>

                    <div className="card-group ">
                        {this.state.tags.map(tag => {
                            return (
                                <button type="button" className="btn btn-secondary  m-1" onClick={() => { this.selectTag(tag); }}>{tag}</button>
                            );
                        })}
                    </div>

                    <div className='d-flex flex-row align-items-center mt-4 w-50'>
                        <input className="form-control mx-2" type="search" placeholder="Search" aria-label="Search" name="text" onChange={this.handleChange}  value={text ? text : ''}/>
                    </div>

                </div>

                <div className="container ">
                    <main className="bg-white  ">
                        <div className="row" id='datos'>
                            {this.state.news.map((news, index) => {
                                return (
                                    <div className="col mb-4 col-md-6 col-lg-4" key={index}>
                                        <div className="card">
                                            <div className="card-body">
                                                <p className="card-text" key={news._id}>{news.pubDate}</p>
                                                <h5 className="card-title">{news.title}</h5>
                                                <p className="card-text">{this.trimData(news.content)}</p>
                                                <a target="_blank" className="btn btn-secondary btn-sm " id="button-login"
                                                    href={news.link} >See
                                                    News</a>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })}


                        </div>
                    </main>
                </div>


                <Footer />
            </div>
        )
    }

}