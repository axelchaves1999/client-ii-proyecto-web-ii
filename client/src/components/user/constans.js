module.exports = {
    apiUrl: 'http://localhost:4000/',
    queries: {
        getNewsBySearch: `
        query($newsSearchText: String!, $newsSearchUserId: ID!){
            newsSearch(text: $newsSearchText, User_id: $newsSearchUserId) {
                title,
                content,
                link,
                pubDate 
            }
        }
        `,
        getNewsByTag: `query($newsTagText: String!, $newsTagUserId: ID!){
            newsTag(text: $newsTagText, User_id: $newsTagUserId) {
                title,
                content,
                link,
                pubDate
            }
        }`,
        getNews: `
        query($newsUserId: ID!){
            news(User_id: $newsUserId) {  
                title,
                content,
                link,
                pubDate

            }
        }
        `
            
    }
}